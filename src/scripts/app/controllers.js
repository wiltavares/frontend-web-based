'use strict';

define([
  'angular',
  'services',
  'app/modules/controllers/pesquisa.controller',
  'app/modules/controllers/cadastro.controller'],
  function (
    angular,
    services,
    PesquisaController,
    CadastroController) {

    var _m = angular.module('myApp.controllers', ['myApp.services']);
    _m.controller('PesquisaController', PesquisaController);
    _m.controller('CadastroController', CadastroController);
    return _m;

  }
);