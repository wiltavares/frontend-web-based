'use strict';

define([
   'angular',
   'services',
   'controllers',
   'angularRoute',
   'ngResource'
], function (angular) {

    // Declare app level module which depends on filters, and services
    
    return angular.module('myApp', [
        'ngRoute',
        'ngResource',
        'myApp.services',
        'myApp.controllers'
    ]);
});
