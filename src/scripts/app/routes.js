'use strict';
define(['angular', 'app/app'], function(angular, app) {
  return app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/pesquisa', {
      templateUrl: 'views/pesquisa.html',
      controller: 'PesquisaController'
    })
    .when('/cadastro', {
      templateUrl: 'views/cadastro.html',
      controller: 'CadastroController'
    })
    .when('/', {
      templateUrl: 'views/pesquisa.html'
    })
    .otherwise({
      redirectTo: '/index.html'
    });
  }]);
});
