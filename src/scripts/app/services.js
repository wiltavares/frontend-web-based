'use strict';
var deps = [
  'angular'
  ,'app/modules/services/cadastro.service'
  ,'app/modules/services/pesquisa.service'];
  define(deps, function (
    angular,
    cadastroService,
    pesquisaService) {

    /* Services */
    // Demonstrate how to register services
    // In this case it is a simple value service.
    var _m = angular.module('myApp.services', []);
    _m.factory('$cadastroService', cadastroService);
    _m.factory('$pesquisaService', pesquisaService);
    return _m;

  }
);
