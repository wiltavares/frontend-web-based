require.config({    
    paths:{
        jquery: "lib/jquery-3.2.1",
        angular: "lib/angular",
        angularRoute: "lib/angular-route/angular-route.min",
        ngResource: "lib/angular-resource/angular-resource",
        services : "app/services",
        controllers : "app/controllers",
        materialDesign: "lib/material-design-lite/material.min",
        sweetAlert: "lib/sweetalert" //Alertas
    },
    shim: {
        angular: ['jquery'],
        ngResource:['angular'],
        toaster: ['angular', 'jquery', 'ngAnimate'],
        ngAnimate: ['jquery', 'angular'],
        'angular' : {'exports' : 'angular'},
        'angularRoute': ['angular'],
        'ngRoute' : {'exports' : 'angularRoute'},
        controllers: ['services', 'sweetAlert'],
        services: ['angular', 'ngResource'],
        'jquery': {
            exports: '$'
        }
    },
    priority: [
        "angular"
    ]
});

var deps = ["angular", "app/app", "app/routes", "app/configs"];

require(deps, function(angular, app, routes, configs){
    
    angular.bootstrap(document, [app['name']]);
});